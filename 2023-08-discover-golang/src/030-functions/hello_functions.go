package main

import (
	"fmt"
)

/*
 * A simple function to add two integers
 */
func Add(x int, y int) int {
	return x + y
}

/*
 * Another version for parameters with the same type
 */
func AddV2(x, y int) int {
	return x + y
}

/*
 * A variadic function can be called with any number of trailing argument
 */
func AddVariadic(nums ...int) int {
	// nums is []int
	total := 0
	for _, x := range nums {
		total += x
	}
	return total
}

/*
 * Functions can return functions
 */
func makeIncrementer(i int) func(int) int {
	var f = func(v int) int {
		return v + i
	}
	return f
}

/*
 * Go supports anonymous functions, which can form closures
 */
func intSeq() func() int {
	i := 0
	return func() int {
		i++
		return i
	}
}

/*
 * A recursive factorial implementation
 */
func fact(n int) int {
	if n == 0 {
		return 1
	}
	return n * fact(n-1)
}

/*
 * A recursive closure just for 'flexing'
 */
func whatIsHappeningHere() {
	var fib func(n int) int
	fib = func(n int) int {
		if n < 2 {
			return n
		}
		return fib(n-1) + fib(n-2)
	}
	fmt.Println(fib(7)) // => 13
}

/*
 * Multiple return values
 */
func vals() (int, int) {
	return 3, 7
}

func main() {
	fmt.Println(Add(2, 3)) // => 5

	fmt.Println(AddVariadic(1, 2, 3)) // => 6

	incrementWith5 := makeIncrementer(5)
	fmt.Println(incrementWith5(10)) // => 15

	nextInt := intSeq()
	fmt.Println(nextInt()) // => 1
	fmt.Println(nextInt()) // => 2
	fmt.Println(nextInt()) // => 3

	fmt.Println(fact(7)) // => 5040

	whatIsHappeningHere()

	// multiple return vals
	a, b := vals()
	fmt.Println(a)
	fmt.Println(b)
	_, c := vals()
	fmt.Println(c)
}
