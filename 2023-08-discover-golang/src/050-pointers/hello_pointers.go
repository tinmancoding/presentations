package main

import (
	"fmt"
)

func strUpdaterValue(s string) {
	s = "s is just a copy, we cannot access the original variable"
}

func strUpdaterPointer(s *string) {
	*s = "changed"
}
func main() {
	changeMe := "Initial message."

	strUpdaterValue(changeMe)
	fmt.Println(changeMe) // no change

	strUpdaterPointer(&changeMe)
	fmt.Println(changeMe) // => "changed"

	// address of changeMe
	fmt.Println(&changeMe)

	// a string pointer
	var sp *string
	sp = &changeMe

	// Accessing the value
	*sp = "changed again"
	fmt.Println(changeMe) // => changed again
}
