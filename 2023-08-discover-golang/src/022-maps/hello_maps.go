package main

import (
	"fmt"
)

func simpleMap() {
	var firstMap = map[string]int{
		"foo": 1,
		"bar": 2,
		"baz": 3,
	}
	fmt.Println(firstMap["bar"]) // => 2

	firstMap["baz"] = 10
	firstMap["yolo"] = -1
	fmt.Println(firstMap)
}

func emptyMap() {
	var emptyMap map[string]string

	// emptyMap is not initialized we cannot add elements to it!!!
	if emptyMap == nil {
		fmt.Println("nil!")
	}

	emptyMap = make(map[string]string) // built-in function make to the rescue
	emptyMap["a"] = "A"
	fmt.Println(emptyMap)

	// another way to initialize the map:
	var anotherMap = map[string]string{}
	anotherMap["a"] = "A"
	fmt.Println(anotherMap)

	// initialize with initial capacity
	// By specifying an initial capacity, you can avoid frequent reallocation and improve performance when you know the approximate size of the map.

	thirdMap := make(map[string]string, 100)
	fmt.Println(thirdMap) // => map[]

}

func mutatingMaps() {
	m := make(map[string]int)

	m["Answer"] = 42
	fmt.Println("The value:", m["Answer"]) // => The value: 42

	m["Answer"] = 48
	fmt.Println("The value:", m["Answer"]) // => The value: 48

	delete(m, "Answer")
	fmt.Println("The value:", m["Answer"]) // => The value: 0

	v, ok := m["Answer"]
	fmt.Println("The value:", v, "Present?", ok) // => The value: 0 Present? false
}

func main() {
	simpleMap()
	emptyMap()
	mutatingMaps()
}
