package main

import (
	"fmt"
)

func printValueAndType(v interface{}) {
	fmt.Printf("Value: %v Type: %T \n", v, v)
}

var packageLevelVariable int

func numbers() {
	// integers (int, uint, int8, int16, int32, int64, uint8, uint16, uint32, uint64)
	var i int = 42
	var j uint = 456
	var useJust8BitsPls int8 = 127

	printValueAndType(i)
	printValueAndType(j)
	printValueAndType(useJust8BitsPls)
	/*
		Value: 42 Type: int
		Value: 456 Type: uint
		Value: 127 Type: int8
	*/

	// floats (32 or 64)
	var pi float64 = 3.14159265359
	printValueAndType(pi)
	/*
		Value: 3.14159265359 Type: float64
	*/

	// the type is optional
	var thisIsAnotherVar = 128
	var thisMustBeAFloat = 123.45
	printValueAndType(thisIsAnotherVar)
	printValueAndType(thisMustBeAFloat)
	/*
		    Value: 128 Type: int
			Value: 123.45 Type: float64
	*/

	// the initial value is optional
	var myUnknownInteger int
	printValueAndType(myUnknownInteger) // this is 0

	// shorthand syntax only inside functions

	myVariable := 234.456
	printValueAndType(myVariable)
	/*
		Value: 234.456 Type: float64
	*/

	// no implicit type conversions!

	var result float64
	var a int = 10
	var b float64 = pi
	// result = a * b // not compile: mismatched types int and float64
	result = float64(a) * b
	printValueAndType(result)
	/*
		Value: 31.4159265359 Type: float64
	*/
}

func strings() {
	var source string = "Budapest"
	destination := "Москва" // utf-8

	printValueAndType(source)
	printValueAndType(destination)
	printValueAndType(source + " - " + destination)
}

func booleans() {
	var isThisTrue = true
	var maybe bool // this is false by default
	printValueAndType(isThisTrue)
	printValueAndType(maybe)
	/*
		Value: true Type: bool
		Value: false Type: bool
	*/

	printValueAndType(!isThisTrue)         // not
	printValueAndType(isThisTrue && maybe) // and
	printValueAndType(isThisTrue || maybe) // or
	/*
		Value: false Type: bool
		Value: false Type: bool
		Value: true Type: bool
	*/
}

func main() {
	// simple types representing numbers
	fmt.Println("Numbers")
	numbers()
	// strings
	fmt.Println("Strings")
	strings()
	// booleans
	fmt.Println("Booleans")
	booleans()
}
