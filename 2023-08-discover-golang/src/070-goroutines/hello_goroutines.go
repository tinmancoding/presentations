package main

import (
	"fmt"
	"strings"
	"time"
	// "sync"
)

// var wg sync.WaitGroup

func slowlyPrint(id int, s string) {
	// defer wg.Done()

	for _, world := range strings.Fields(s) {
		time.Sleep(time.Second)
		fmt.Printf("%v: %v\n", id, world)
	}
}

func main() {
	// wg.Add(1)
	slowlyPrint(1, "First sentence we need to print.")
	// wg.Add(1)
	slowlyPrint(2, "This is the second one.")

	// wg.Wait()
}
