package main

import (
	"fmt"
	"strings"
)

func countWordFrequencies(text string) map[string]int {
	words := strings.Fields(text)
	frequencies := make(map[string]int)

	for _, word := range words {
		frequencies[word]++
	}

	return frequencies
}

func main() {
	document := `
	Go programs are organized into packages.
	A package is a collection of source files in the same directory that are compiled together.
	Functions, types, variables, and constants defined in one source file are visible to all other source files within
	the same package.
	A repository contains one or more modules.
	A module is a collection of related Go packages that are released together.
	A Go repository typically contains only one module, located at the root of the repository.
`
	frequencies := countWordFrequencies(document)
	fmt.Println(frequencies)
	fmt.Println("Hello World!")
}
