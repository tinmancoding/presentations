package main

import (
	"fmt"
)

func main() {
	var lotteryNumbers = [5]uint{1, 2, 3, 4, 5}
	fmt.Println(lotteryNumbers) // => [1 2 3 4 5]

	var emptyArray [10]int
	fmt.Println(emptyArray) // => [0 0 0 0 0 0 0 0 0 0]

	arrayWithInferredLength := [...]string{"This", "is", "an", "array", "of", "strings"}
	fmt.Println(len(arrayWithInferredLength)) // => 6

	// accessing element at the index

	fmt.Println(lotteryNumbers[0]) // => 1

	arrayWithInferredLength[1] = "was"
	fmt.Println(arrayWithInferredLength) // => [This was an array of strings]

	// Multi-dimensional arrays

	var multiplicationTable [10][10]int
	for i := 1; i <= 10; i++ {
		for j := 1; j <= 10; j++ {
			multiplicationTable[i-1][j-1] = i * j
		}
	}
	fmt.Println(multiplicationTable) // => [[1 2 3 4 5 6 7 8 9 10] [2 4 6 8 10 12 14 16 18 20] ... ]

	// Partially initialized arrays

	firstFivePrimeNumbers := [5]uint{2, 3}
	fmt.Println(firstFivePrimeNumbers) // => [2 3 0 0 0]

	// Note:
	// "An array's length is part of its type, so arrays cannot be resized.
	// This seems limiting, but don't worry; Go provides a convenient way of working with arrays."

}
