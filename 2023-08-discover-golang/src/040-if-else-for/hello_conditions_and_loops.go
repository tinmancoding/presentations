package main

import (
	"fmt"
)

func forLoopClassic() {
	for i := 0; i < 5; i++ {
		fmt.Println(i)
	}
}

func forLoopWithJustCondition() {
	i := 0
	for i < 5 {
		fmt.Println(i)
		i++
	}
}

func forLoopWithRange() {
	mySlice := []string{"A", "B", "C", "D", "E"}

	for index, value := range mySlice {
		fmt.Println(index, value)
	}

	myMap := map[string]string{
		"foo": "bar",
		"baz": "yoo",
	}
	for key, value := range myMap {
		fmt.Println(key, value)
	}

}

func guessTheNumber(number int) {
	var guess int
	for {
		fmt.Print("Guess the number:")
		fmt.Scanln(&guess)

		if guess == number {
			fmt.Println("That's it!")
			break
		} else if guess > number {
			fmt.Println("Wrong! It's smaller.")
		} else {
			fmt.Println("Wrong! It's larger.")
		}
	}
}

func main() {
	//guessTheNumber(42)

	forLoopClassic()
	forLoopWithJustCondition()
	forLoopWithRange()

	guessTheNumber(42) // infinite loop with conditional break
}
