package main

import (
	"fmt"
	"math"
)

type Person struct {
	firstName string
	lastName  string
	age       int
}

type Musician struct {
	Person
	instrument string
}

// method defined as value receiver
func (p Person) displayName() string {
	return p.firstName + " " + p.lastName
}

// method defined as pointer receiver
func (p *Person) addYear(year int) {
	p.age += year // Note: No need to use the pointer dereference eg. (*p).age
}

func examplesWithPerson() {
	bFleck := Musician{
		Person: Person{firstName: "Bela", lastName: "Fleck", age: 64},
		instrument: "banjo",
	}

	bFleck.addYear(1)
	fmt.Println(bFleck) // => {{Bela Fleck 65} banjo}

	persons := []Person{
		Person{firstName: "John", lastName: "Black", age: 42},
		Person{firstName: "Steve", lastName: "Example", age: 18},
	}
	persons = append(persons, bFleck.Person)
	fmt.Println(persons) // => [{John Black 42} {Steve Example 18} {Bela Fleck 65}]

	for _, person := range persons {
		fmt.Println(person.displayName())
	}
}

// Shapes with area

type Circle struct {
	r float64
}

func (c Circle) area() float64 {
	return (22 / 7.0) * (c.r * c.r)
}

type Triangle struct {
	a, b, c float64
}

func (t Triangle) area() float64 {
	s := (t.a + t.b + t.c) / 2
	return math.Sqrt(s * (s - t.a) * (s - t.b) * (s - t.c))
}

type Rectangle struct {
	a, b float64
}

func (r Rectangle) area() float64 {
	return r.a * r.b
}

type Shape interface {
	area() float64
}

func printArea(s Shape) {
	fmt.Println(s.area())
}

func examplesWithShapes() {
	t := Triangle{3, 4, 5}
	r := Rectangle{a: 3, b: 4}
	c := Circle{r: 5}
	printArea(t)
	printArea(r)
	printArea(c)
}

func main() {
	examplesWithPerson()
	examplesWithShapes()
}
