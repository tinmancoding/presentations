package main

import (
	"fmt"
)

func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}

func simpleSlices() {
	primes := [6]int{2, 3, 5, 7, 11, 13}

	var q []int = primes[1:4] // A slice
	fmt.Println(q)            // => [3 5 7]

	// A slice does not store any data, it just describes a section of an underlying array.

	colors := [4]string{"red", "green", "blue", "black"}

	lastTwoColors := colors[2:4]
	lastTwoColors[0] = "yellow"

	fmt.Println(colors) // => [red green yellow black]
}

func sliceLiterals() {
	//  this creates an array as above, then builds a slice that references it
	s := []int{2, 3, 5, 7, 11, 13}
	printSlice(s) // => len=6 cap=6 [2 3 5 7 11 13]

	// The length of a slice is the number of elements it contains.
	// The capacity of a slice is the number of elements in the underlying array,
	// counting from the first element in the slice.

	// Slice the slice to give it zero length.
	s = s[:0]
	printSlice(s) // => len=0 cap=6 []

	// Extend its length.
	s = s[:4]
	printSlice(s) // => len=4 cap=6 [2 3 5 7]

	// Drop its first two values.
	s = s[2:]
	printSlice(s) // => len=2 cap=4 [5 7]
}

func zeroValue() {
	// The zero value of a slice is nil.

	var e []int
	printSlice(e) // => len=0 cap=0 []
	if e == nil {
		fmt.Println("nil!") // => nil!
	}

}

func dynamicallySized() {
	a := make([]int, 5)
	printSlice(a) // => len=5 cap=5 [0 0 0 0 0]

	b := make([]int, 0, 5)
	printSlice(b) // => len=0 cap=5 []

	c := b[:2]
	printSlice(c) // => len=2 cap=5 [0 0]

	d := c[2:5]
	printSlice(d) // => len=3 cap=3 [0 0 0]
}

func appendingToSlice() {
	var s []int
	printSlice(s)

	// append works on nil slices.
	s = append(s, 0)
	printSlice(s)

	// The slice grows as needed.
	s = append(s, 1)
	printSlice(s)

	// We can add more than one element at a time.
	s = append(s, 2, 3, 4)
	printSlice(s)

	/* =>
	   len=0 cap=0 []
	   len=1 cap=1 [0]
	   len=2 cap=2 [0 1]
	   len=5 cap=6 [0 1 2 3 4]
	*/
}

func main() {
	simpleSlices()
	sliceLiterals()
	zeroValue()
	dynamicallySized()
	appendingToSlice()
}
